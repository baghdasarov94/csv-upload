<?php
session_start();

class LoginController {
    const USERNAME = 'admin';
    const PASSWORD = 'ykJeI9fU5Q';


    public function login()
    {
        $this->checkCredentials();

        $_SESSION['auth'] = true;

        $this->back();
    }

    public function logout(){
        session_destroy();

        $this->back();
    }

    private function checkCredentials()
    {
        if (!isset($_POST['username']) || !isset($_POST['password'])) {
            $this->back();
        }

        if ($_POST['username'] !== self::USERNAME || $_POST['password'] !== self::PASSWORD) {
            $this->back();
        }
    }

    public function back()
    {
        header('Location: /index.php');
        die();
    }

}

$controller = new LoginController();

if (isset($_GET['logout'])) {
    $controller->logout();
}

$controller->login();

function dd(...$args)
{
    http_response_code(500);

    foreach ($args as $x) {
        var_dump($x);

    }
    die(1);
}
