<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('xdebug.max_nesting_level', -1);

session_start();

if(!isset($_SESSION["auth"]) || $_SESSION["auth"] !== true){
    header("location: login-form.php");
    exit;
}

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

Class Upload
{
    public function store()
    {
        $sheetData = $this->validator($_FILES);
        if (empty($sheetData)) $this->back();
        unset($sheetData[0]);

        if (isset($_POST['output_type']) && $_POST['output_type'] == 'txt') {
            $file = fopen("output.txt", "w") or die("Unable to open file!");
            $txt =  "FileType = active listings,  Version = 1.2.1 (Please do not edit this line or this will result in error when uploading)\n";
            $txt .=  "Channel\tSellerSKU\tYour Price\tShipping\tCost\tFBM Shipping Cost\tGroup\n";
            fwrite($file, $txt);

            foreach ($sheetData as $item) {
                $price = $this->calculate($item);

                $txt =  "Amazon US\t".$item['0']."\t\t\t".$price."\t\t\n";
                fwrite($file, $txt);
            }

            fclose($file);

            header("Content-Disposition: attachment; filename=output.txt");
            header("Content-Type: application/force-download");
            readfile('output.txt');
            die();
        } else {
            $spreadsheet = new Spreadsheet();
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A1', 'MSKU')
                ->setCellValue('B1', 'Price');

            $col = 2;
            foreach ($sheetData as $item) {
                $price = $this->calculate($item);

                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValueByColumnAndRow(1, $col, $item['0'])
                    ->setCellValueByColumnAndRow(2, $col, $price);

                $spreadsheet->getActiveSheet()
                    ->getStyleByColumnAndRow(2, $col)
                    ->getNumberFormat()
                    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

                $col++;
            }


            $writer = new Xlsx($spreadsheet);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="output.xlsx"');
            $writer->save('php://output');
            die();
        }
    }

    private function validator($files)
    {
        $file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        if(isset($files['file']['name']) && in_array($files['file']['type'], $file_mimes)) {

            $arr_file = explode('.', $files['file']['name']);
            $extension = end($arr_file);

            if('csv' == $extension) {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            } else {
                return [];
            }

            $spreadsheet = $reader->load($files['file']['tmp_name']);

            $sheetData = $spreadsheet->getActiveSheet()->toArray();

            return $sheetData;
        }

        return [];
    }

    private function calculate($item)
    {
        if (! is_numeric($item['4'])) $item['4'] = intval($item['4']);
        if (! is_numeric($item['9'])) $item['9'] = intval($item['9']);
        if (! is_numeric($item['10'])) $item['10'] = intval($item['10']);
        if (! is_numeric($item['11'])) $item['11'] = intval($item['11']);
        if (! is_numeric($item['12'])) $item['12'] = intval($item['12']);

        return number_format($item['4'] + $item['9'] + $item['10'] + $item['11'] + $item['12'], 2, '.', ',');
    }

    public function back()
    {
        header('Location: /');
        die();
    }
}

$controller = new Upload();
if (isset($_FILES['file']) && $_SERVER['REQUEST_METHOD'] === 'POST') {
    $controller->store();
}else {
    $controller->back();
}

/**
 * Dump the passed variables and end the script.
 *
 * @param  mixed  $args
 * @return void
 */
function dd(...$args)
{
    http_response_code(500);

    foreach ($args as $x) {
        var_dump($x);

    }
    die(1);
}
